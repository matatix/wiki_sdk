# Tiled Map Editor

Dans cette section, nous parlerons du logiciel **Tiled**, puissant outil d'édition et de création de Cartes qui grâce au [Tiled2rxdata](https://gitlab.com/NuriYuri/tiled2rxdata) de Yuri permet de créer ses Cartes efficacement, avec **une infinité de calques**, puis de les convertir pour les rendre utilisables avec **PSDK**.

## Liste des articles

- [Installer Tiled](install.md)
- [Bien Commencer](getting_started.md)