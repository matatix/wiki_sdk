# Programmer pour PSDK

## Notice

Dans PSDK, les méthodes de programmation sont un peu différentes de celles d'un projet RMXP classique. En effet, PSDK utilise des versions de Ruby ultérieures à la version 2.5 alors que RMXP se cantonne à la version 1.8 contenue dans le RGSS. PSDK possède également une base script séparée de la base script de l'utilisateur car PSDK se fonde sur des fichiers Ruby plutôt qu'un gros fichier monolithique Scripts.rxdata. Cela permet à PSDK d'utiliser Git, par exemple.

![tip](info "Ruby 2.5 est au moins 4 fois plus rapide et possède des nouvelles fonctionnalités par rapport à Ruby 1.8.")

Il est fortement recommandé de savoir un peu programmer en Ruby avant de programmer pour PSDK. Dans cette optique, on peut citer deux tutoriels :
- [Tutorials Point (en anglais)](https://www.tutorialspoint.com/ruby/index.htm)
- [Nuri Yuri (en version française)](https://gitlab.com/NuriYuri/tutoriel_ruby/blob/master/fr/0_Table_Des_Matières.md)

![tip](danger "Ne suivez en aucun cas les tutoriels Ruby d'Essentials, même si Marin ou Lukas l'ont fait. Ils sont potentiellement mauvais car Essentials ne suit pas vraiment les règles de base de Ruby (l'utilisation du snake case par exemple) et fonctionne avec Ruby 1.8 ce qui peut amener à des mauvaises pratiques en Ruby 2.5.")

## Index

Dans cette section du Wiki, nous aborderons différents sujets donc vous  aurez besoin d'être sur cet index afin d'accéder aux index des autres sujets (le sélecteur de pages ne montrera pas les-dits sujets).

Ici sont consignés tous les topics réalisés dans la section Programmation:
- [Configurer l'environnement de programmation](fr/programming/env/index.md)
- [Quelques règles de programmation](fr/programming/scr.md)
- [Comment ça marche](fr/programming/hdiw/index.md) : Cette section expliquera comme fonctionne certains systèmes de PSDK.