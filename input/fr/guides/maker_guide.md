# Le Guide du Fangame Maker

## Préface

Si vous lisez ce topic, c'est que vous venez d'arriver dans la communauté de Pokémon Workshop, et pas par hasard : vous voulez créer un fangame Pokémon. Il y a seulement un tout petit hic : vous ne savez pas par quoi commencer, ce qu'il faut faire, comment le faire, avec quoi le faire... On a vite fait de se perdre dans toutes les recommandations que l'on peut nous donner.

![tip](warning "Avant toute chose, un petit avertissement : la création d'un Fangame Pokémon n'est pas une science exacte et nombreuses sont les méthodes pour arriver à sortir votre jeu.")

Elles ne se valent pas toutes, bien évidemment, mais elles auront certainement le mérite de vous faire arriver au bout, moyennant une motivation stable tout au long du développement de votre fangame. Dans ce tutoriel, nous vous décrirons une de ces méthodes. Elle n'a pas la prétention d'être la meilleure qui soit, mais elle aura au moins le mérite de ne pas être la pire. Libre à vous de la modifier selon vos besoins et vos envies.

### Un petit mot avant de commencer

Ce tutoriel n'a pas pour but de vous apprendre à vous servir des outils nécessaires à la création d'un jeu Pokémon. Dans la mesure du possible, nous essaierons de vous fournir des liens utiles sur lesquels il faudra vous appuyer.

## Les compétences à développer

Tout d’abord, sachez que quelle que soit la tâche que vous effectuerez, il vous faudra développer certaines compétences à sa réalisation. Créer un Fangame Pokémon ne fait pas exception à la règle. Dans ce chapitre aux allures de catalogue, nous vous expliquerons ce que nous jugeons être des compétences/qualités requises et importantes dans le développement d’un Fangame Pokémon.

### Gestion du temps, motivation et curiosité

Le titre est, nous l'espérons, suffisamment explicite. Un projet, c'est un investissement de temps considérable. Sachez qu’à l’heure où vous lisez ces lignes, il existe des fangames qui sont en développement depuis facilement 7 ans et qui continuent d’être développés aujourd’hui pour un jour être totalement finis. Il vous faudra donc être d'une patience à toute épreuve. Ce n’est pas parce que vous n’arrivez pas à maîtriser quelque chose le jour même qu’il en sera de même le lendemain. Persévérez.

On ne le dit jamais assez, mais il est inutile de rusher votre apprentissage et de sauter des étapes. Comme on ne démarre pas une voiture directement à la 6ème vitesse, vous ne pouvez pas espérer démarrer d'un coup d'un seul sur les chapeaux de roues. Prenez votre temps et allez à votre rythme. Même si vous mettez un an à sortir une démo (ce qui en soit sera déjà un bel accomplissement), vous aurez à minima le mérite d'un travail de longue haleine sur lequel vous aurez persévéré. Vous aurez donc compris qu'il vous faut être motivé, sinon ça ne marchera pas.

Pour finir, la curiosité sera votre meilleur atout dans le domaine du Fangame making. Intéressez-vous à tout, testez tout, et surtout faites des recherches avant de poser vos questions. Vous en ressortirez grandis !

### Patience et autonomie, des qualités précieuses trop peu répandues

Non, ce sous-chapitre ne sera pas une critique de la société. Nous n'allons pas dire que les gens de nos jours sont des feignasses et qu'ils veulent tout dans la seconde. Cependant, une chose est sûre : une grande partie des nouveaux arrivants ne savent pas faire une recherche décente. On ne vous en veut bien évidemment pas, nous avons tous été pareil à un moment. C'est pourquoi nous vous invitons à donner le meilleur de vous-même afin d'apprendre à être patient et autonome. Car oui, ces qualités se cultivent. Mais vous allez certainement nous demander :

![tip](info "Okay mais ça va me servir à quoi tout ça ? Si j'attends qu'on me réponde à ma question, j'perds du temps non ?")

Ce à quoi nous répondons que c'est bien évidemment contre cette idée qu'il faut lutter. Voyez-vous, la création d'un Fangame c'est un gigantesque puzzle de 4000 pièces. Si vous n'arrivez pas à trouver la pièce qui correspond à l'ensemble que vous venez de débuter, vous passez à un autre ensemble et de fil en aiguille ce sont tous ces ensembles qui donnent votre puzzle fini. Dans le cas de votre jeu, c'est la même chose. N'attendez pas bêtement qu'on vous réponde. Si quelqu'un vous répond dans l'heure, tant mieux, sinon passez juste à autre chose et ne perdez pas de temps. La patience et l'autonomie.

Attendre une réponse sagement et prendre l'initiative d'avancer sur un autre morceau de votre projet. Car oui, parfois la réponse pourra prendre du temps à arriver, car après tout nous avons tous une vie et il est donc parfois impossible de vous répondre directement. Il est déjà arrivé qu'une bonne semaine s'écoule avant qu'une réponse soit donnée, alors ne perdez pas cette semaine, c'est important.

## Suivre un schéma de travail bien pensé

Vous en conviendrez, on ne peut commencer à travailler sans avoir un plan d'action à toute épreuve. Vous avez déjà vu des ouvriers se mettre à travailler sur un chantier sans les plans de l'architecte ? Internet regorge de photos de chantiers complètement loufoques, on vous laissera chercher par vous-même. Avec votre projet, ce sera pareil. Dans ce chapitre, nous allons justement vous donner quelques astuces pour lancer la machine des idées et vous permettre de créer les fondations de votre jeu.

### Poser ses idées à plat

Maintenant que vous avez, en théorie, établi vos limites créatives, prenez le temps de vous poser et de mettre en ordre vos idées. L'exercice n'est pas bien compliqué. Commencez par écrire ce qui vous passe par la tête, ce qui concerne l'histoire de votre jeu, la région, ce que vous voulez voir dedans. A ce moment de la création, laissez sortir toutes les idées qui vous passent par la tête. Oui, vraiment tout. On reviendra sur l’intérêt de ces idées par la suite.

### S'imposer des limites dès le départ

Oui, vous avez bien lu. Le meilleur moyen de commencer son projet, c'est de vous imposer des limites. Il y a toute une logique derrière cette affirmation. En effet, plus votre projet est ambitieux, plus le travail à réaliser sera conséquent. Il faut évidemment bien plus de temps pour faire un projet avec 8 régions, 2000 Pokémon et 64 badges que pour faire un jeu avec simplement une nouvelle région et un Pokédex de 200 Pokémon.

![tip](danger "Si vous venez de débuter, ne faites pas un projet avec 8 régions, SÉRIEUSEMENT")

Ainsi, limiter votre folie créative sera le meilleur moyen pour vous d'accomplir quelque chose. Et comme l'a dit un grand sage : “c'est de la limitation que naît la créativité” – SirMalo, 2020.

### Le grand nettoyage

Vous devriez, à cette étape de la lecture, avoir une TONNE d’idées en tout genre... Il va être nécessaire de faire le tri dans ces idées. Comme dit précédemment, il n’est pas intéressant de garder une idée qui prendrait trop de temps à adapter. Gardez l’essentiel ! Pour chaque idée, voyez à les mettre en relation avec les autres idées déjà validées. "Est-ce que cette idée en particulier amènera quelque chose à votre projet ? Sera-t-elle simple à intégrer ? Le contexte de votre projet se prête t-il à cette idée ?" Vous devrez vous questionner à ce moment précis, et agir en accord avec les limites que vous vous êtes fixé.

## RMXP, PSDK, Ruby Host, kézako ?

Nos ancêtres le savaient pertinemment : il n’y a pas de fumée sans feu. Pour votre Fangame Pokémon, c'est pareil, il vous faut des outils adaptés. On les compte au nombre de 4. Trois sont obligatoires, et le dernier est facultatif mais très utile, on y reviendra. On parlera donc dans cette partie de RMXP, PSDK et Ruby Host. Et vous allez certainement nous demander :

![tip](info "Ouais d'accord mais c'est quoi tous ces logiciels ?")

Patience, nous y venons. :)

### RMXP, la base de la création

RMXP, de son vrai nom RPG Maker XP, est un logiciel payant qui permet de créer des JRPG. Vous pourrez et devrez l'acheter sur Steam (vous y êtes tenu par la loi après tout) où il y est d'ailleurs régulièrement en solde. Il possède de nombreuses fonctionnalités permettant aux "makers" (c'est le nom qu'on donne aux utilisateurs de RMXP et de PSDK sur Pokémon Workshop) de créer leur jeu assez facilement sans avoir à programmer, même s'il est totalement possible de programmer en apprenant le langage de programmation Ruby.
Parmi les fonctionnalités offertes par le logiciel, on retrouve un éditeur de base de données, un éditeur de cartes, un éditeur d'événements (une suite d'actions pré-programmées permettant de donner vie à votre jeu), etc.

![tip](warning "Vous éviterez de confondre une map et une worldmap : la première est là où le joueur se déplace, la deuxième est la carte du monde.")

Il vous est NÉCESSAIRE de le maîtriser. Nous dirons même plus : PRIMORDIAL. Nous reparlerons de son apprentissage dans le chapitre 4. Pour le moment, tout ce que vous avez besoin de savoir, c'est que RMXP est la base sur laquelle repose PSDK.

### PSDK, le coeur de votre projet Pokémon

Mais alors, qu'est-ce donc que PSDK ? Acronyme de Pokemon Starter Development Kit, c'est un projet RMXP dont la base script a subit des modifications titanesques pour que votre jeu puisse se comporter comme un jeu Pokémon (ou au moins qu'il soit le plus ressemblant possible, des libertés ont parfois été prises).

PSDK est donc la base de script/graphismes/audio/base de données. Attention cependant : avec PSDK, certaines choses continuent de fonctionner avec RMXP et d'autres passent par un intermédiaire. Ainsi, les events fonctionnent toujours avec l'éditeur RMXP et les maps se font toujours avec RMXP (même s'il existe une nouvelle méthode dont nous parlerons plus tard). L'exemple le plus concret est celui de la BDD : une partie de l'édition de la BDD RMXP a été délocalisée au profit d'une solution plus flexible et adaptée nommée Ruby Host.

Pour apprendre à vous servir de PSDK, nous vous conseillons le Wiki, section [Event-Making](https://psdk.pokemonworkshop.fr/wiki/fr/event_making/index.html)
Une démo est aussi incluse dans PSDK. Nous vous conseillons de l'inspecter en profondeur.

### Ruby Host, pour une base de données propre et encadrée

Ruby Host est l'outil d'édition de base de données fournie conjointement avec PSDK. Sans Ruby Host, il vous est impossible de modifier les données de votre jeu (données des Pokémon, des attaques, des objets, etc).
Il comporte de nombreuses options que vous pourrez directement découvrir en suivant le lien donné ci-dessous.

Pour apprendre à l'utiliser, nous vous conseillons le Wiki, section [Ruby Host](https://psdk.pokemonworkshop.fr/wiki/fr/ruby_host/index.html)

## Comment organiser son travail ?

Maintenant que la vision que vous avez de votre projet n'est plus un amalgame d'idées farfelues et floues, et que vous connaissez les outils qui vous aideront à développer votre jeu, vous allez pouvoir passer aux choses sérieuses. Le but va être d'établir l'ordre dans lequel faire les choses.

Dans ce guide, nous allons vous donner ce qui est pour nous l'un des déroulements les plus logiques. Vous pouvez le suivre aveuglément, le modifier à votre goût ou bien partir sur tout autre chose, le choix est vôtre. Sachez cependant que la création d'un Fangame n'a rien d'inné. C'est un long processus d'apprentissage et d'adaptation. Sans repartir dans les détails, rappelez-vous que motivation, patience et curiosité seront vos meilleurs alliées.

Sans plus attendre, notre méthode :

### Maîtriser les outils

Comme dit plus tôt, pour faire un bon jeu, il faut de bons outils. Une fois qu'on a les bons outils en sa possession, il faut savoir s'en servir. Et pour savoir s'en servir, il n'y a pas 36 solutions, il faut apprendre. Dans ce sous-chapitre, nous allons revenir sur les outils importants qui vous aideront tout au long de votre processus de création et qu’il vous sera nécessaire d’apprendre.

#### Retour sur RMXP

Comme dit précédemment, il est primordial que vous sachiez vous servir de RMXP. Plusieurs possibilités s’offrent à vous :

- si vous êtes plutôt bon en anglais (ce qui de nos jours ne devrait plus être un luxe tant l’anglais est important) vous pouvez chercher des tutoriels vidéos sur YouTube. La communauté anglaise RMXP est généralement plutôt productive et vous ne devriez pas avoir trop de mal à trouver votre bonheur.
- si votre niveau d’anglais laisse à désirer, il existe un tutoriel sur [Final-RPG](https://www.final-rpg.com/rpg_maker_xp-tutoriels.html) qui vous permettra de vous faire la main sur le logiciel.
- bidouiller, car oui, on apprend mieux en mettant les mains dans la pâte ! Nous vous conseillons de créer un petit projet RMXP (sans PSDK !) et de tester toutes les commandes, tous les boutons, bref TOUT. Ne faites pas d’impasse, vous gagnerez un temps précieux en faisant cela.

![tip](warning "Quel que soit la méthode que vous choisirez, sachez que vous DEVREZ savoir vous servir de RMXP, et c'est non négociable.")

#### GIMP ou GraphicsGale, pour éditer vos images/faire du pixel-art

Au cours de votre processus créatif, vous vous rendrez compte d’une chose : il vous manquera TOUJOURS une ressource, une image, etc… C’est pourquoi nous vous conseillons d’apprendre à vous servir de GIMP ou Photoshop puis de GraphicsGale ou d’Aseprite. Ce sont d’excellents outils. Les deux premiers sont surtout adaptés pour de la retouche d’image (même s’il est possible de faire du pixel-art dessus) et les deux derniers sont des logiciels spécialisés dans le pixel-art ! Il existe bien évidemment de très nombreux tutoriels en ligne, que ça soit sur l’utilisation des logiciels ou sur comment faire du pixel-art. Sur ce dernier point, pareil, ça ne s’apprend pas en un jour, ne baissez jamais les bras ! ;)

Cet outil est déjà plus facultatif mais a tout de même un intérêt.

### Mettre en place le background de votre jeu

Dans cette partie, nous parlerons plus en détail du background de votre jeu, comprenez son histoire, son univers, ses personnages, ses quêtes et défis, ses problématiques… En bref, ce qui fait de l’univers de votre jeu un univers vivant. En partant d’un ensemble très large pour finalement arriver aux plus petits détails, nous nous assurerons de fournir un schéma de travail efficace. Cependant, soyons d’accord de suite : écrire est très facile… Bien écrire est très compliqué (mais pas insurmontable).

#### Le lore de votre région, le pilier central de votre jeu

Bien évidemment, dans un univers comme celui de Pokémon, l’ensemble le plus large que l’on puisse nommer est la Région (le Monde en lui-même est un ensemble encore plus grand mais il demande de décupler le travail à fournir, nous nous arrêterons donc simplement à la Région) et ainsi, il est important d’avoir une solide idée de ce qu’on veut véhiculer avec celle-ci.

Si une région se doit de diffuser des sentiments de désarroi et de tristesse ou de solitude, des marais, des forêts sombres et des villes dévastées pourraient faire l’affaire, de même pour des environnements de déserts. A l’inverse, des forêts claires et luxuriantes, des prairies vertes d’herbe, des petits villages de campagne ou des petits villes côtières peuvent transmettre de la chaleur, une impression de vie et d’autres sentiments positifs.

En clair, ce que vous ferez de votre région aura un impact sur sa lecture par le joueur, autant dans sa représentation littérale comme sa représentation graphique. De même, avoir un lore dont la valence émotionnelle tend vers le positif dans une région marquée très négativement pourra surprendre le joueur (en bien comme en mal, tout dépend de comment vous construisez votre lore).

#### La trame principale

Une fois votre région parfaitement établie, et son lore associé construit et longuement médité, il est grand temps de passer au morceau le plus compliqué. En effet, construire une histoire ne revient pas simplement à écrire en continu sans réfléchir. Tout d’abord, ne commencez pas à écrire si vous ne savez pas d’où vous partez, et vers où vous allez. Il vous faut être certain de votre début, et savoir quelle fin vous souhaitez amener.

Mais ce n’est pas tout. Pensez à retourner régulièrement en arrière : à force d’écrire vers l’avant on se met à rapidement oublier ce qu’il s’est passé précédemment, et on introduit inconsciemment des erreurs qu’on appelle dans le milieu des plot-holes (comprenez des trous scénaristiques qui montrent les faiblesses structurelles de votre histoire). Évitez tous ces processus de tunnel-vision (le fait de ne toujours regarder que dans une direction) et votre histoire n’en sera que meilleure.

#### Les personnages principaux et leurs intérêts

Dans toute bonne histoire ils y a des personnages forts et/ou attachants, ou qui au contraire arrivent à attiser toute la colère du joueur. Nous ne fournirons pas ici une démarche complète de leur création, Google contient de très nombreuses références sur comment écrire des personnages. Pour le moment, voici quelques conseils en vrac qui sont généralement des conseils toujours utiles :

- votre personnage n’est pas vous, faites en sorte de ne pas vous insérer en un personnage au risque d’en faire un personnage Marie Sue (le personnage parfait car lissé au possible et donc sans aucun défaut)
- ne faites pas de votre personnage un cliché ambulant : vous pouvez jouer dessus mais prévoyez une grosse marge pour son développement
- utilisez à bon escient les archétypes de personnages et privilégiez ceux qui apporteront de la surprise aux joueurs

![tip](warning "Cette liste n’est absolument pas exhaustive et il vous reviendra de chercher comment rendre vos personnages plus vivants.")

#### Les éléments secondaires

Cependant, votre monde et votre histoire ne tournent pas uniquement autour de vos personnages principaux. Il est important de montrer que votre monde vit au-delà du premier plan. Diverses manières sont à votre disposition :

- les quêtes secondaires qui permettent d'ancrer le joueur et son personnage dans la dynamique de la région (vous pouvez le pousser à explorer tout en lui offrant des récompenses ou lui apprendre de nouvelles mécaniques de jeu)
- les explorations pour en apprendre plus sur le lore et renforcer l'immersion du joueur dans votre univers
- les PNJ pour donner un côté plus humain à votre région…

Ces 3 manières peuvent être utilisées indépendamment ou conjointement, cependant gardez à l'esprit qu'elles sont à utiliser avec parcimonie. Vos joueurs s'en rendront vite compte si vous utilisez ces méthodes non pas pour étendre votre jeu mais pour combler certaines faiblesses scénaristiques.

### Réfléchir à l'aspect graphique de votre jeu

L’un des points qui attirera en premier l’oeil de la plupart des joueurs, ce sera l’aspect graphique de votre projet. Concrètement, il faut que votre jeu garde le plus possible une cohérence dans les différentes ressources qui vont être utilisées. Pour caricaturer, vous allez difficilement réussir à utiliser des characters (petits sprites qui représenteront les personnages sur vos maps) issus des versions Rouge et Bleu avec des éléments de décors tirés des jeux sortis sur DS.

Plus généralement, pour obtenir un rendu agréable et harmonieux, essayez de trouver un style qui vous plaît, qui contient tous les éléments dont vous pourriez avoir besoin, et n’en déviez pas.

#### Choisir parmi les grands styles

Il vous faut commencer par opter pour un style de “génération”. Votre jeu va-t-il adopter le style graphique des versions Verte, Rouge et Bleue, celui de Diamant, Perle et Platine ou bien celui de Noire et Blanche ? Chaque nouvelle génération de jeu apporte en effet un style graphique nouveau, avec ses propres façons de “dessiner” les bâtiments, les personnages, etc.
Les générations peuvent être triées ainsi :

- 1G : Vert, Rouge, Bleu et Jaune (attention aux versions en noir et blanc et celles issues de la GameBoy Color, avec une palette en couleurs !).
- 2G : Or, Argent et Cristal. Les graphismes de ces jeux gardent globalement les mêmes proportions et couleurs que ceux de la 1G, et il est donc possible de les mélanger. La 2G est toutefois légèrement plus détaillée que la 1G.
- 3G : Rubis, Saphir et Émeraude + Rouge-Feu et Vert-Feuille. Si les proportions entre ces deux groupes de jeux sont globalement les mêmes, il faut tout de même noter que les jeux RFVF ont une palette légèrement plus vive et des characters différents. Un mélange est possible, mais avec quelques ajustements et en prenant garde à bien utiliser des characters qui proviennent exclusivement d’un jeu ou d’un autre. L’avantage de cette génération est que le travail d’extraction des ressources de jeux GBA est assez facile et que leur quasi totalité est ainsi disponible sur internet.
- 4G : Diamant, Perle et Platine + HeartGold et SoulSilver. Tout comme pour la 3G, les ressources sont très similaires entre ces deux ensembles de jeux. Les versions HGSS vont néanmoins utiliser une palette plus vive que DPPt. Si vous mélangez les unes avec les autres, un petit coup de recoloration (en utilisant des palettes issues des versions que vous avez choisies) permettra de vous assurer un rendu harmonieux. Tout comme pour la 3G, beaucoup de ressources de la 4G ont été rippées (à savoir extraites des jeux officiels) et sont disponibles en ligne, notamment ici : [Ressources 4G](https://pokemonworkshop.fr/forum/index.php?topic=5530.0).
- 5G : Noire et Blanche + Noire 2 et Blanche 2. Ce sont les seuls jeux de la 5ème génération et ils ont exactement le même style. Contrairement à ce que certains pourraient croire, les proportions et palettes ne sont pas les mêmes que celles de la 4G. La 5G va avoir tendance à utiliser des éléments un peu plus grands et surtout plus détaillés. Il est donc déconseillé de mélanger ces styles, sauf à de très rares occasions éventuellement. C’est un style qui a été relativement bien rippé et qui peut aisément être utilisé pour un fangame qui voudrait un rendu plus détaillé et moins cartoon que la 4G.
- 6G, 7G et 8G : je regroupe ces styles car, bien que tous différents, ils marquent l’entrée dans l’ère de la full-3D et il est donc impossible de les utiliser tels quels dans un projet en 2D. Quelques tentatives d’adaptation en 2D existent bien çà et là, mais elles sont très peu nombreuses.

Dans tous les cas, faites toujours en sorte de bien déceler les différences qui existent entre ces différents styles, ou à tout le moins de bien vérifier de quelles versions proviennent des ressources que vous voulez utiliser. Cela vous évitera des mélanges hasardeux qui ne feront que donner un côté amateur et précipité à votre jeu.

#### Les styles alternatifs, pour & contre

Outre les ressources officielles, vous aurez sans doute envie d’utiliser des éléments graphiques “custom”, c’est-à-dire créés par des amateurs. Ces créations vous permettront de vous démarquer, de trouver des tiles ou des personnages que vous ne trouveriez pas dans les jeux officiels, ou tout simplement d’utiliser un style qui correspondra mieux à vos goûts.

De grands noms des ressources custom reviennent souvent : Magiscarf, Kyledove ou encore SailorVicious… Chacun a son style, bien particulier, qui colle également à une génération donnée. Ainsi, les tiles de Kyledeove sont majoritairement de style 4G, par exemple.

Mais cela risque de vous rajouter une contrainte supplémentaire : en plus de devoir convenir au style générationnel de vos ressources, les graphismes custom ne peuvent être mélangés entre eux ou avec des éléments tirés des jeux officiels. En effet, ce qui fait la force, mais aussi la faiblesse, de ces ressources, c’est qu’elles vont utiliser des palettes graphiques et des méthodes de dessin uniques.

Si vous faites le choix des styles custom, réfléchissez bien ! Faites attention de bien regarder que tous les éléments dont vous aurez besoin sont disponibles, ou faites en sortes d’apprendre à reproduire ce style pour faire vous-mêmes vos ajouts.

Et bien évidemment, assurez-vous que tout soit libre de droit ! Si ce n’est pas expressément écrit, alors c’est que ça ne l’est sûrement pas. Dans ce cas, demandez directement à l’auteur, par respect pour son travail.

#### La cohérence graphique de votre projet, un point capital

Dans tous les cas, le fait de travailler longuement sur la cohérence graphique de votre jeu jouera considérablement sur l’image que les joueurs en auront. N’oubliez pas que les graphismes sont parmi les premières choses qui retiendront leur attention.

Faire des efforts pour faire en sorte que tous les éléments que vous afficherez à l’écran soient le plus harmonieux possible créera un liant qui fera que tout le reste tiendra bien en place. Et chaque détail compte : vos tiles ont-ils des contours ou non ? De quelle couleur sont vos ombres ? Quelle taille font vos personnages en jeu ? Utilisez-vous plutôt une palette vive ou pastel ?

Le fait même de vous poser ces questions montrera que vous accordez une importance particulière à votre projet et que vous ne prenez pas les choses à la légère.

### Préparer des croquis de vos maps et de votre région

Il est l’heure pour vous de vous improviser cartographe ! Dans cette section, nous vous donnerons des conseils sur la création de votre worldmap et de vos maps, afin de garder une cohérence dans votre jeu !

#### Commencer par la worldmap pour savoir d'où vous partez

Le titre parle pour lui-même. Commencez par dessiner votre worldmap pour savoir dans quelle direction vous irez ! Si vous avez suivi ce tutoriel correctement, vous devriez avoir un début de Lore pour votre région. Si c’est le cas, ce Lore vous permettra d’orienter votre génie créatif dans la bonne direction.

Que ça soit la présence d’un lac millénaire figé dans le temps ou une grande forêt d’arbres dont les feuilles brillent des 7 couleurs de l’arc-en-ciel, votre map devra refléter ces environnements et les mettre en valeur. N’hésitez pas à étudier les worldmaps des jeux officiels, elles sont une mine d’or de game-design et level-design.

Vous pouvez d’abord commencer par la faire au crayon sur feuille à main levé pour vous donner une idée globale de la forme, puis par la suite vous pouvez la scanner pour l’obtenir sur ordinateur afin de repasser par dessus… Ce choix est vôtre, mais faites en sorte de toujours rester dans la même cohérence graphique !

#### Schématiser et conceptualiser les maps, un travail de longue haleine

Une fois votre worldmap terminée, il vous faut maintenant l’esthétique principale de votre jeu : les maps. En prenant en compte l’ensemble des conseils évoqués dans le chapitre concernant l’aspect graphique de votre jeu, il va falloir commencer à préparer vos maps, en cohérence avec tout ce que vous avez fait jusqu’ici.

Votre histoire, votre lore, votre worldmap, tous ces éléments dicteront grandement ce qu’elle deviendra. N’hésitez pas à d’abord coucher sur papier ce qui fait son identité, puis à dessiner un schéma, même grossier. Vous ne serez ni les premiers, ni les derniers à faire des schémas Paint, et ils auront le mérite d’orienter votre génie créatif et de vous donner un premier avant-goût de ce que sera votre map. Avoir une première vision de la chose permet d’appliquer des corrections plus facilement, mais aussi de s’accorder en fonction du style graphique choisi. Une fois tout cela réalisé, il ne vous reste plus qu’à mapper !

Rappelez-vous également que les maps remplissent de nombreux rôles. Elles ont tout d’abord un aspect esthétique, bien sûr, et il est important que la majorité d’entre elles puissent se démarquer les unes des autres grâce à un concept fort qui vous permettrait de la décrire en une phrase. Il est aussi primordial de penser à ce que le joueur verra de votre map en jeu. N’oubliez pas qu’il n’en aura jamais une vision d’ensemble, mais qu’il ne pourra la voir que dans un espace d’environ 20 tiles de large pour 15 tiles de haut. Il ne faut surtout pas qu’il se retrouve dans des zones trop vides ou trop chargées.

Mais il est aussi important de penser à l’aspect gameplay de votre map. Elle doit guider le joueur pour le faire avancer, faire en sorte qu’il puisse s’entraîner, explorer, trouver des objets, voire faire avancer le scénario… Tout cela suppose une importante réflexion en amont et une bonne connaissance du déroulé global de votre jeu.

## Pour aller plus loin…

Ce chapitre, qui grandira au fur et à mesure des nouvelles interrogations de la communauté, servira à parler de différents points “facultatifs” mais qui méritent malgré tout une attention particulière.

### Tiled, pour un mapping sans prise de tête

Nous avons parlé un peu plus tôt de l'éditeur de maps de RMXP, et laissé sous-entendre qu'une autre méthode pour le mapping existe. C'est le cas. Il vous est possible de faire vos maps sur un éditeur de map spécialisé UNIQUEMENT dans le mapping : son nom est Tiled. Tiled est de prime abord un tantinet compliqué à utiliser, mais une fois l'apprentissage effectué, il se révèle bien plus puissant que RMXP en terme de mapping (nombre de couche illimité, facilité dans les méthodes de mapping, tilesets illimités, automapping…).

![tip](warning "Sachez cependant que le convertisseur est actuellement sujet à quelques légers soucis mais nous avons pour projet de retaper un jour cet aspect de la compatibilité Tiled -> RMXP.")

Il existe un tuto vidéo sur la chaîne YouTube de Nuri Yuri : [Vidéo Tuto](https://www.youtube.com/watch?v=zoNvegXQfUk), et un tutoriel écrit par le staff devrait prochainement faire surface. Les liens seront mis à jour en temps et en heure.

### Monter une équipe, une nécessité ?

On a tous connu ce membre qui vient d’arriver dans la communauté et qui, sans travail approfondi sur le scénario, sans définition d’une réelle direction artistique, souhaite recruter des développeurs pour monter une équipe de 15 personnes...

La constitution d’une équipe est quelque chose de complexe, tout comme son maintien. Recrutement, management, gestion des tâches, sont autant de disciplines qui demandent une certaine maturité et certaines compétences. Impossible d’imaginer un chef de projet qui ne s’implique pas et se contente de confier des tâches.

À la question “est-il nécessaire de monter une équipe pour mener à bien son projet ?”, il convient de répondre que oui, mais seulement lorsqu’on a de solides acquis, une vision claire de son projet et une connaissance approfondie du rôle de chacun. Les membres doivent pouvoir être conseillés, dirigés, valorisés dans leur travail. Difficile d’imaginer conseiller un Pixel Artiste sans notions dans le domaine.

Mais ne vous découragez pas, prenez le temps de bien maîtriser tous les outils à votre disposition, d’apprendre à apprendre, en vous amusant. Sachez qu’il existe de nombreuses autres solutions pour travailler à plusieurs et optimiser ses chances de mener à bien son projet. Notamment les échanges de ressources, les collaborations, les fameux “art trade” (où un artiste A réalise une création pour un artiste B et vice et versa), les commissions, et bien plus encore.

Si vous vous sentez l’âme d’un véritable Chef de Projet, pensez à lire ce Vadémécum, spécialement fait pour vous : [Vadémécum du chef de Projet](https://pokemonworkshop.fr/2019/03/01/vademecum-du-chef-de-projet/).

### Apprendre Ruby, dans quel contexte ?

PSDK est programmé en Ruby, qui est un langage de programmation orienté-objet (si le terme ne vous dit rien, je vous conseille ce lien : Programmation orientée objet — Wikipédia). Il sera nécessaire pour vous de l’apprendre si la simple modification des images ne suffit pas à donner l’interface que vous voulez, ou encore si vous voulez faire une interface de zéro, un système, ou quoi que ce soit qui requiert plus que la simple connaissance de RMXP et des commandes de script.

Pour apprendre le Ruby, nous vous conseillons le tutoriel vidéo sur le Ruby par la chaîne YouTube Grafikart (Attention, il existe aussi un tutoriel Ruby on Rails, il ne vous concerne pas). Nuri Yuri a aussi écrit un tutoriel que vous pouvez retrouver ici : [Tuto Ruby](https://gitlab.com/NuriYuri/tutoriel_ruby)
