# Faire un Move Tutor Spécialisé

Il n'existe pour l'instant pas de commande appelant facilement un move tutor à la manière des jeux officiels. Il existe cependant un moyen de s'en rapprocher et même, pour ceux qui en auront la foi, de faire quelque chose de similaire. Je dis de ce Move Tutor qu'il est "spécialisé" parce qu'il apprend une capacité bien définie à un Pokémon bien défini.

Dans notre cas, le PNL auquel je m'adresse souhaitera apprendre Écras Face à un Galopa, et me demandera de sélectionner un Pokémon de mon équipe. Si je sélectionne bien un Galopa ne connaissant pas Écras Face, il lui proposera effectivement de l'apprendre.

Voici un screen de mon event :
![ScreenMove|center](img/event_making/galopa_ecrasface_movetutorspe.png)

J'utilise d'abord le script `call_party_menu` qui me permet de sélectionner un Pokémon puis de le stocker grâce à `@pokemon = $actors[gv[Yuki::Var::Party_Menu_Sel]]`.

Je vérifie ensuite que le joueur a bien sélectionné un Pokémon en vérifiant que la variable `43` est bien supérieure ou égale à 0.

Je vérifie ensuite que le joueur a bien sélectionné un Galopa grâce à `@pokemon&.db_symbol == 78`

Je vérifie enfin que son Galopa ne connait pas déjà Écras Face grâce à `@pokemon&.has_skill?(:pound)`

Je lui apprends enfin la capacité, avec `skill_learn(gv[Yuki::Var::Party_Menu_Sel], 1)`, `gv[Yuki::Var::Party_Menu_Sel]` étant la variable dans laquelle je l'ai stocké et `1` l'ID de la capacité à enseigner.

À partir de tout ça, je pense que de nombreuses portes vous seront dores et déjà ouvertes pour des move tutors et des aprentissages originaux de certaines capacités dans vos fangames !