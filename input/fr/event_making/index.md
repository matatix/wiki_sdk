# Event Making

Cette section du Wiki contient tous les tutoriels relatif à l'évent Making sous Pokémon SDK. 

Plusieurs informations (parfois très basiques) y seront regroupées.

Les **commandes de base** :
- [Afficher un message](messages.md)
- [Afficher un choix](choices.md)

Les commandes relatives aux **Événements** :
- [Modifier les attributs des évènements](event_attribute.md)

Les commandes relatives aux **Pokémon** :
- [Donner un Pokémon](give_pokemon.md)
- [Lâcher un pokémon errant](roaming_pokemon.md)
- [Créer un magasin de Pokémon](create_pokemon_mart.md)
- [Créer un choix de Starter Pokémon](create_starter_choice.md)

Les commandes relatives aux **Objets** :
- [Donner un objet](give_item.md)
- [Activer le Pokédex](enable_pokedex.md)
- [Créer un magasin](create_mart.md)
- [Donner un badge](give_badge.md)

Les commandes relatives aux **Personnages Non Jouables (PNJ)** :
- [Créer un centre Pokémon](create_pokemon_center.md)
- [Move Tutor Spécialisé](move_tutor_specialise.md)

Les commandes relatives aux **Combats contre les Pokémon sauvages** :
- [Démarrer un combat de Pokémon](start_wild_battle.md)

Les commandes relatives aux **Combats contre les Dresseurs** :
- [Créer un dresseur](trainer_event.md)
- [Combats scénarisés](scenarized_battles.md)

Les commandes relatives aux **Quêtes** :
- [Gérer les quêtes](quest.md)

Les commandes relatives aux **Systèmes** :
- [Utiliser le FollowMe](followme.md)
- [Système de Temps](time-system.md)
- [Lancer le Panthéon](hall_of_fame.md)
- [Lancer/Configurer le Mining Game](mining_game.md)
