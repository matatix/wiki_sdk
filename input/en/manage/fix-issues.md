# Fix issues

This page list some known issue you can have using PSDK.

## RubyFPS are low

Usually in PSDK, RubyFPS should always be higher than 200FPS. If your hardware is good but you RubyFPS are low, ask yourself this question: Am I running PSDK under a Nvidia Graphic Card ?   
If the answer is **yes**, then there's an easy fix for this issue:

![Nvidia Control Panel|Center](img/nvidia.png "Disable Threaded Optimization")

## The game doesn't start

Sometime, the game can crash during the starting process. If that happen, we cannot help **unless you do the following procedure**:

1. Double click on `cmd.bat` in your project folder (`cmd` if extensions are not visible).
2. Write the command `game debug` and press enter.
3. Wait until you're able to write commands on the console again.
4. Click in the dark area of the console, press `CTRL+A` two time and press `CTRL+C` (this should copy the whole console content)
5. Open Notepad (`Window+R` => `notepad`)
6. Paste the content of the console using `CTRL+V`
7. Save the file and send it to use in the channel `#en-psdk-support`

We'll tell you what's wrong.
